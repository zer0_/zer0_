/*

EXAMPLE DATA === EXAMPLE DATA === EXAMPLE DATA === 

var expenses_db = {
    "groceries": {
        "amount": 32.43,
        "frequentlyUsed": 1,
        "emoji": "🧀🌶"
    },
    "transport": {
        "amount": 52.62,
        "frequentlyUsed": 1,
        "emoji": "🍽"
    },
    "going out": {
        "amount": 119.99,
        "frequentlyUsed": 1,
        "emoji": "🚃"
    },
} */

let expenses_db = {}

// =====================================
// =====================================
// =====================================


function deleteAllDataPermanently() { /// CAUTION!!!!! THIS FUNCTION WILL DELETE ALL USER DATA == FOREVER!
    console.log('User requested for data to be deleted');
    if(confirm("Do you really want to DELETE ALL DATA?")) {
        if(confirm("DATA CANNOT BE RESTORED! Do you want to proceed?")) {
            localStorage.removeItem('expensesDb');
            console.log('Data was deleted');

            $('#totalAmount').html('$0.0'); // resets total amount to be zero
            $('#expensesList').html(''); // Cleans out the entire expenses list
        }
    }
}


function resetFormFields() { // resets the fields to enter amount and category
    $("#userAmountField").val('');
    $("#userCategoryField").val(''); 
}

function updateTotalSum() { // calculates the new total sum for the month and adds it in
    let sum = 0.0;
    for (var key in expenses_db) {
        sum += expenses_db[key]['amount'];
    }
    let newAm = '$' + sum;
    $('#totalAmount').html(newAm);
}

function updateDetailArea() { // this function recalculates and repaints the detail area
    
    $('#expensesList').html(''); // this removes all existing elements from the expensesList - otherwise, the elements will just keep piling on and double-display themselves
    
    for (let key in expenses_db) {
        let newCategory = key[0].toUpperCase() + key.substring(1);
        let newAmount = '$' + expenses_db[key]['amount'];
        let emojis = expenses_db[key]['emoji']
        $('#row_template .categoryField').html(newCategory);
        $('#row_template .emoji').html(emojis);
        $('#row_template .amountField').html(newAmount);
        
        let newRowElement = $("#row_template").clone();
        newRowElement.attr('id',key); // reset ID of the element to a new value - or else, the id will remain as row_template
        newRowElement.removeClass('d-none'); // the row_template element is not displayed by default, so we need to remove that class

        $('#expensesList').append(newRowElement);
    }
}

function updateFrequentlyUsed() { // this function calculates which are the most frequently used categories
    console.log('Frequently used is not yet implemented')
}

function saveToLocalStorage() {
    localStorage.setItem('expensesDb', JSON.stringify(expenses_db)); // JSON.stringify() is required since localStorage cannot store objects, only strings.
}

function updateAll() {
    resetFormFields(); // RESET the form fields
    updateTotalSum(); // UPDATE total amount
    updateDetailArea(); // UPDATE the details... area
    updateFrequentlyUsed();
}

function save(amount_user_entered, category_user_entered) {
    console.log("OLD:");
    console.log(expenses_db);
    console.log("=================");
    console.log("=================");
    console.log("=================");
    console.log("The amount entered is");
    console.log(amount_user_entered);

    let user_category = category_user_entered.toLowerCase(); // to prevent confusion, I should always work in lowercase with the stored categories
    
    if(user_category in expenses_db) { // checks to see whether the category is an existing category in the expenses_db
        let old_expenses = expenses_db[user_category]['amount'];
        let new_amount = (isNaN(old_expenses) ? 0.0 : old_expenses) + amount_user_entered; // this is to ensure that we do not run into issues if the 'amount' is somehow undefined/NaN
        expenses_db[user_category]['amount'] = new_amount;
    }
    else { // else, it means that this is a newly entered category, so we need to append a new entry
        let data = {
            "amount": amount_user_entered,
            "frequentlyUsed": 1,
            "emoji": "🧀🌶"
        }
        expenses_db[user_category] = data;
    }

    saveToLocalStorage(); // stores the newly entered data in localStorage as well

    console.log("NEW:");
    console.log(expenses_db);

    updateAll();
}

$(document).ready(function() {

    if(localStorage.getItem('expensesDb') === null) { // this means that the user visits the website for the very first time
        $('#totalAmount').html('$0.0'); // set up the total amount to be $0
    }
    else { // if localStorage item exists, let's retrieve it and store it as DB
        expenses_db = JSON.parse(localStorage.getItem('expensesDb')); // JSON.parse() is required because localStorage only stores strings, not objects
    }

    updateAll(); // first to initiliase the total amount when the page is first loaded

    $('.amountQuickButtons').click(function() {
        let value = parseFloat(this.value);
        let user_entered = isNaN(parseFloat($("#userAmountField").val())) ? 0.0 : parseFloat($("#userAmountField").val()); //if the user has NOT YET entered an amount, .val() will be NaN, which would cause issues when adding it to let value - therefore, we check isNaN first.
        $("#userAmountField").val(value+user_entered);
    });

    $('.categoryQuickButtons').click(function() {
        const clickedCategory = $(this).find('.categoryName').html();
        $('#userCategoryField').val(clickedCategory);
    });

    $('#saveButton').click(function() {
        let user_amount = parseFloat($("#userAmountField").val());
        let user_category = $("#userCategoryField").val();
        save(user_amount, user_category);
    });
});